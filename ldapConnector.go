package	main

import	(
	"log"
	"crypto/tls"
	"github.com/go-ldap/ldap/v3"
)

func main() {
	tlsConfig := &tls.Config{InsecureSkipVerify: true}
	ldapURL := "dc02.uca.local:636"
	l, err := ldap.DialTLS("tcp", ldapURL, tlsConfig)
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
	err = l.Bind("cn=trmartinek1,dc=uca,dc=local", "password")
	if err != nil {
		log.Fatal(err)
	}
}
