package	main

import	(
	"encoding/json"
	"fmt"
	"net/http"
	"log"
	"io/ioutil"
)

type Data struct {
	Identifier	string
	Password	string
}

type DataLogin struct {
	Identifier	string
	Password	string
}

func handleRequestRoot(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("body read KO")
	}
	var data Data
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Printf("deserialis KO")
	}
	fmt.Printf("%+v\n", data)
}

func handleRequestLogin(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("body read KO")
	}
	var data Data
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Printf("deserialis KO")
	}
	fmt.Printf("%+v\n", data)
}

func main() {
	mainORM()
	http.HandleFunc("/", handleRequestRoot)
	http.HandleFunc("/login", handleRequestLogin)
	//http.HandleFunc("/", handleRequestRoot)
	//http.HandleFunc("/", handleRequestRoot)
	//http.HandleFunc("/", handleRequestRoot)
	//http.HandleFunc("/", handleRequestRoot)
	log.Fatal(http.ListenAndServe(":8088", nil))
}
