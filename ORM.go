package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

func mainORM() {
	connStr := "user=ada password='' dbname=dbada sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		fmt.Println("Erreur lors de la connexion à la base de données :", err)
		return
	}
	defer db.Close()
	rows, err := db.Query("SELECT * FROM logins")
	if err != nil {
		fmt.Println("Erreur lors de l'exécution de la requête :", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var colonne1 string
		var colonne2 string
		err = rows.Scan(&colonne1, &colonne2)
		if err != nil {
			fmt.Println("Erreur lors de la récupération des données :", err)
			return
		}
		fmt.Println("login:", colonne1, "password:", colonne2)
	}
	if err = rows.Err(); err != nil {
		fmt.Println("Erreur lors de la récupération des données :", err)
	}
}


